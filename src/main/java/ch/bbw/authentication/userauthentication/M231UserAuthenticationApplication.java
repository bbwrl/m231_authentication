package ch.bbw.authentication.userauthentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class M231UserAuthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(M231UserAuthenticationApplication.class, args);
	}

}
